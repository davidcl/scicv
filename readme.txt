// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

README of Scilab Computer Vision Module
----------------------------------------

This toolbox is a binding from the OpenCV library (in version 2.4.13).
Please see www.opencv.org for further details on the OpenCV library.

This toolbox is currently in version 0.4.

To load and initialize the toolbox:
- cd <path to the toolbox folder>
- exec loader.sce
- scicv_Init()
