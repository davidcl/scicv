<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab Computer Vision Module
 * Copyright (C) 2017 - Scilab Enterprises
 *
 -->
<refentry xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg"  xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab"
  xml:lang="fr" xml:id="sub">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>subtract</refname>
    <refpurpose>Substract one image from another</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Syntax</title>
    <synopsis>img = sub(img1, img2[, mask[, dtype]])</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>img1</term>
        <listitem>
          <para>Image (<link linkend="Mat">Mat</link>).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>img2</term>
        <listitem>
          <para>Image (<link linkend="Mat">Mat</link>).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>mask</term>
        <listitem>
          <para>Optional mask image (<link linkend="Mat">Mat</link>).</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>dtype</term>
        <listitem>
          <para>Optional depth of the output image.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>img</term>
        <listitem>
          <para>Output image (<link linkend="Mat">Mat</link>).</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><function>sub</function> computes the difference of two images pixel per pixel.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    scicv_Init();

    img1 = imread(getSampleImage("lena.jpg"));
    img2 = new_Mat(size(img1, 'c'), size(img1, 'r'), CV_8UC3, 40);

    img_out = subtract(img1, img2);
    matplot(img_out);

    delete_Mat(img1);
    delete_Mat(img2);
    delete_Mat(img_out);
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See also</title>
    <simplelist type="inline">
      <member><link linkend="add">add</link></member>
    </simplelist>
  </refsection>
</refentry>