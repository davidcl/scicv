// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

// <-- CLI SHELL MODE -->

scicv_Init();

img = imread(getSampleImage("puffin.png"), CV_LOAD_IMAGE_GRAYSCALE);
assert_checkequal(Mat_channels(img), 1);

[res, img_out] = threshold(img, 127, 255, THRESH_BINARY);

assert_checkfalse(Mat_empty(img_out));

delete_Mat(img);
delete_Mat(img_out);
