// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

// <-- CLI SHELL MODE -->

scicv_Init();

img_gray = imread(getSampleImage("shapes.png"), CV_LOAD_IMAGE_GRAYSCALE);

thresh = 100;
img_canny = Canny(img_gray, thresh, thresh*2, 3);

[img_contours, contours] = findContours(img_canny, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, [0, 0]);
assert_checkfalse(Mat_empty(img_contours));
assert_checkequal(typeof(contours), "PtLists");

delete_Mat(img_gray);
delete_Mat(img_contours);

