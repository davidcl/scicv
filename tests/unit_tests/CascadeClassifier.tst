// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

// <-- CLI SHELL MODE -->

scicv_Init();

img = imread(getSampleImage("faces.jpg"));
cascadeClassifier = new_CascadeClassifier();

res = CascadeClassifier_load(cascadeClassifier, "data/haarcascades/haarcascade_frontalface_alt.xml");
assert_checktrue(res);

faces = CascadeClassifier_detectMultiScale(cascadeClassifier, img, 1.2, 2, CV_HAAR_SCALE_IMAGE, [30 30]);
assert_checkequal(size(faces), 17);

delete_CascadeClassifier(cascadeClassifier);

delete_Mat(img);
