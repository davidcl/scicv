// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

%{
#undef SKIP_INCLUDES
#include "opencv2/contrib/contrib.hpp"
%}

%include opencv_contrib_ignore.i

%include "opencv2/contrib/contrib.hpp"
