// Scilab Computer Vision Module
// Copyright (C) 2017 - Scilab Enterprises

%include InputOutputArray_typemaps.i
%include InputArrayOfArrays_typemaps.i
%include OutputArrayOfArrays_typemaps.i
%include InputArray_typemaps.i
%include OutputArray_typemaps.i

%include PointPtr_typemaps.i
%include Point_typemaps.i
%include Point2f_typemaps.i
%include Rect_typemaps.i
%include Size_typemaps.i
%include Scalar_typemaps.i
%include KeyPoints_typemaps.i
%include VectorRect_typemaps.i

%include intPtr_typemaps.i
%include float_ranges_typemaps.i
%include MatPtr_typemaps.i

%include Mat_typemaps.i
%include PtList_typemaps.i
%include PtLists_typemaps.i

%include MatExpr_typemaps.i
